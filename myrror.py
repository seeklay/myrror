from myrror import myrror

from time import sleep
import logging
import argparse
from util import CustomFormatter

BANNER = """
______  _____  __________ ________ _______ ________
___   |/  /\ \/ /___  __ \___  __ \__  __ \___  __ \\
__  /|_/ / __  / __  /_/ /__  /_/ /_  / / /__  /_/ /
_  /  / /  _  /  _  _, _/ _  _, _/ / /_/ / _  _, _/
/_/  /_/   /_/   /_/ |_|  /_/ |_|  \____/  /_/ |_|
Gaze into the myrror, and what do you see?
"""

def main():

    print(BANNER[1:])
    parser = argparse.ArgumentParser()
    parser.add_argument("-l", help="Listen addr like localhost:25565 or [::1]:25565", type=str, default="127.0.0.1:25565", metavar="addr")
    parser.add_argument("-u", help="Upstream addr like localhost:25565 or [::1]:25565", type=str, default="127.0.0.1:25575", metavar="addr")
    parser.add_argument("-p", help="Outgoing protocol version", type=int, metavar="proto", required=True)
    parser.add_argument("-v", help="If passed, enables verbose logging", action="store_true")
    args = parser.parse_args()

    def host_port(addr: str):
        x = -(addr[::-1].find(':')+1)
        return addr[:x], int(addr[x:][1:])

    logger = logging.getLogger("")
    logger.setLevel(logging.DEBUG if args.v else logging.INFO)
    stdout_handler = logging.StreamHandler()
    stdout_handler.setLevel(logging.DEBUG)
    stdout_handler.setFormatter(CustomFormatter('[%(asctime)s] [%(levelname)s] %(name)s:  %(message)s'))
    logger.addHandler(stdout_handler)

    logger.info("Starting MYRROR...")
    proxy = myrror(host_port(args.l), host_port(args.u), args.p, verbose=args.v)

    while 7:
        try:
            sleep(0.07)
        except KeyboardInterrupt:
            logger.info("Exiting...")
            proxy.shutdown()
            break

if __name__ == "__main__":
    main()
